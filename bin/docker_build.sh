#!/usr/bin/env sh

# required environment variables
# GKE_IMAGE

docker build -t $GKE_IMAGE .
gcloud docker -- push $GKE_IMAGE
