#!/usr/bin/env sh

# required environment variables
# GCP_PROJECT_ID, GCP_PROJECT_ZONE, GCP_SERVICE_ACCOUNT, GCP_CREDENTIALS

# update & install
gcloud --quiet components update
gcloud --quiet components install kubectl

# project
gcloud config set project $GCP_PROJECT_ID
gcloud config set compute/zone $GCP_PROJECT_ZONE

# service account
gcloud auth activate-service-account $GCP_SERVICE_ACCOUNT --key-file=$GCP_CREDENTIALS

# kubernetes
gcloud container clusters get-credentials production
