#!/usr/bin/env sh

# alpine linux
apk add --update make ca-certificates openssl python
wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz
tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false
