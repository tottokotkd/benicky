#!/usr/bin/env sh

# required environment variables
# GKE_IMAGE

kubectl set image deployment/rails rails=$GKE_IMAGE
