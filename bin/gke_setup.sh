#!/usr/bin/env sh

# required environment variables
# GKE_IMAGE

kubectl create -f ./k8s-secret.yaml
kubectl run rails --image=$GKE_IMAGE --port=80
kubectl expose deployment rails --type=LoadBalancer --port=80 --target-port=80
