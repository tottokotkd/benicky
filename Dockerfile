FROM registry.gitlab.com/tottokotkd/benicky/alpine:latest

# Rails App
ENV RAILS_ENV=production
ENV PUMA_SOCKET=/var/run/puma.sock
COPY . /app
RUN rails assets:precompile && rm /app/config/master.key

# Start Server
CMD bundle exec puma -d && \
    /usr/sbin/nginx -c /etc/nginx/nginx.conf

# docker build -t registry.gitlab.com/tottokotkd/benicky:latest .
