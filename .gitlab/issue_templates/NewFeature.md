### 理由・前提

* 今ある問題、新機能のユースケース、いいところ、目的など

### 提案内容

* 何を作る？
* どんな機能を実現する？

/label ~"feature proposal"
/cc @tottokotkd
